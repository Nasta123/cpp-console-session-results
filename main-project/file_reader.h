#ifndef FILE_RE_H
#define FILE_READ_H

#include "session_results.h"

void read(const char* file_name, session_results* array[], int& size);

#endif
