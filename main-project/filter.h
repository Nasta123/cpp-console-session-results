#ifndef FILTER_H
#define FILTER_H

#include "session_results.h"

session_results** filter(session_results* array[], int size, bool (*check)(session_results* element), int& result_size);

/*
�������� ������� <function_name>:
    ������� ���������� ������ � ��������� ������� � ��� ��������� �� ��������,
    ��� ������� ������� ������ ���������� �������� true, ���������� � �����
    ������, ��������� �� ������� ������������ ��������

���������:
    array       - ������ � ��������� �������
    size        - ������ ������� � ��������� �������
    check       - ��������� �� ������� ������.
                  � �������� �������� ����� ��������� ����� �������� ���
                  ������� ������, �������� ������� ������� ����
    result_data - ��������, ������������ �� ������ - ����������, � �������
                  ������� ������� ������ ��������������� �������

������������ ��������
    ��������� �� ������ �� ���������� �� ��������, ��������������� �������
    ������ (��� ������� ������� ������ ���������� true)
*/


bool check_session_results_by_discipline(session_results* element);

/*
�������� ������� check_session_results_by_discipline:
    ������� ������ - ���������, �������� �� �������� ���������� ������� ��������

���������:
    element - ��������� �� �������, ������� ����� ���������

������������ ��������
    true, ���� �������� ���������� ������� ��������, � false � ���� ������
*/


bool check_session_results_by_grade(session_results* element);

/*
�������� ������� check_session_results_by_grade:
    ������� ������ - ���������, �������� �� ������ ���� 7

���������:
    element - ��������� �� �������, ������� ����� ���������

������������ ��������
    true, ���� ������ ���� 7, � false � ���� ������
*/

#endif