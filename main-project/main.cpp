#include <iostream>
#include <iomanip>

using namespace std;

#include "session_results.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"

void output(session_results* subscription)
{
	/********** ����� �������� **********/
	cout << "�������........: ";
	// ����� �������
	cout << subscription->student.last_name << " ";
	// ����� ������ ����� �����
	cout << subscription->student.first_name[0] << ". ";
	// ����� ������ ����� ��������
	cout << subscription->student.middle_name[0] << ".";
	cout << '\n';
	/********** ����� ���������� **********/
	cout << "����������...........: ";
	// ����� ��������
	cout << '"' << subscription->discipline << '"';
	cout << '\n';
	/********** ����� ���� �������� **********/
	// ����� ����
	cout << "���� ��������.....: ";
	cout << setw(4) << setfill('0') << subscription->exam.year << '-';
	// ����� ������
	cout << setw(2) << setfill('0') << subscription->exam.month << '-';
	// ����� �����
	cout << setw(2) << setfill('0') << subscription->exam.day;
	cout << '\n';
	/********** ����� ������ **********/
	// ����� ����
	cout << "������...: ";
	cout <<  subscription->grade;
	cout << '\n';
	cout << '\n';
}

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << "������������ ������ �1. GIT\n";
	cout << "������� �8. ���������� ������\n";
	cout << "�����: ����� �������\n";
	cout << "������: 14\n\n";
	session_results* subscriptions[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("data.txt", subscriptions, size);
		cout << "***** ���������� ������ *****\n\n";
		for (int i = 0; i < size; i++)
		{
			output(subscriptions[i]);
		}
		bool (*check_function)(session_results*) = NULL; // check_function - ��� ��������� �� �������, ������������ �������� ���� bool,
														   // � ����������� � �������� ��������� �������� ���� session_results*
		cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
		cout << "1) ������ � ������ �� ���������� ������� ��������\n";
		cout << "2) ������ �� ����� ���� 7\n";
		cout << "\n������� ����� ���������� ������: ";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_session_results_by_discipline; // ����������� � ��������� �� ������� ��������������� �������
			cout << "***** ������ � ������ �� ���������� ������� �������� *****\n\n";
			break;
		case 2:
			check_function = check_session_results_by_grade; // ����������� � ��������� �� ������� ��������������� �������
			cout << "***** ������ �� ����� ���� 7 *****\n\n";
			break;
		default:
			throw "������������ ����� ������";
		}
		if (check_function)
		{
			int new_size;
			session_results** filtered = filter(subscriptions, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete subscriptions[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}
