#include "filter.h"
#include <cstring>
#include <iostream>

session_results** filter(session_results* array[], int size, bool (*check)(session_results* element), int& result_size)
{
	session_results** result = new session_results * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_session_results_by_discipline(session_results* element)
{
	return strcmp(element->discipline, "������� ��������") == 0;
}

bool check_session_results_by_grade(session_results* element)
{
	return element->grade > 7;
}
